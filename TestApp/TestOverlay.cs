﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Amari;
using Amari.Screens;
using Amari.Input;
using SFML.Graphics;
using Amari.Display;
using Amari.Maths;
using SFML.System;

namespace TestApp
{
    class TestOverlay : Overlay
    {
        Sprite spr = new Sprite(new Texture("02.png"));

        Vector2f Start = new Vector2f(0, 200);
        Vector2f End = new Vector2f(500, 200);
        float t = 0;

        private byte ColorAlpha = 0;

        protected override void Initialize()
        {
            spr.Color = new Color(spr.Color.R, spr.Color.G, spr.Color.B, 1);
            spr.Position = new SFML.System.Vector2f(0, 200);
            ColorAlpha = 0;
            t = 0;
        }

        protected override void ShowAnimation(float DeltaTime)
        {
                if (spr.Color.A < 255)
                {
                    spr.Color = new Color(spr.Color.R, spr.Color.G, spr.Color.B, ColorAlpha);
                    ColorAlpha += Convert.ToByte(500 * DeltaTime);
                
                }
                if (t < 1f)
            {
                t += 1f * DeltaTime;
                spr.Position = Algebra.Lerp(Start, End, t);
            }
                else
            {
                base.ShowAnimation(DeltaTime);
            }
                
        }
        protected override void HideAnimation(float DeltaTime)
        {
            if (spr.Color.A > 0)
            {
                spr.Color = new Color(spr.Color.R, spr.Color.G, spr.Color.B, ColorAlpha);
                ColorAlpha -= Convert.ToByte(500 * DeltaTime);
            }
            if(t > 0f)
            {
                t -= 1f * DeltaTime;
                spr.Position = Algebra.Lerp(Start, End, t);
            }
            else
            {
                base.HideAnimation(DeltaTime);
            }
        }

        public override void Update(float DeltaTime)
        {
            base.Update(DeltaTime);

            if (Core.MouseController.IsButtonClicked(SFML.Window.Mouse.Button.Right)) Hide();

        }

        public override void Draw()
        {
            GameWindow.Current.Draw(spr);
        }

    }
}
