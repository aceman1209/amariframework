﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Amari;
using Amari.Input;

using Amari.Screens;
using Amari.Display;
//using Amari.Animation;
using Amari.UI;

using SFML.System;
using SFML.Graphics;

namespace TestApp
{
    class TestScreen1 : Screen
    {
        TestOverlay TestOverlay = new TestOverlay();
        TestOverlay TestOverlay2 = new TestOverlay();
        TestOverlay TestOverlay3 = new TestOverlay();

        Layer layer = new Layer(new Vector2f(200, 200), new Vector2f(500, 500));

        float t = 0f;
        Button Button;
        Button Button2;
        Button Button3;
        public TestScreen1()
        {
            Initialize();
        }

        protected override void Initialize()
        {
            Name = "test1";
            IsCurrentScreen = true;
            Button = new Button(0.5f, 0.5f, new Vector2f(300, 300), "Overlay", new Font("11613.ttf"), ButtonClick);
            Button2 = new Button(0f, 0f, new Vector2f(300, 300), "Overlay 2", new Font("11613.ttf"), Button2Click);
            Button3 = new Button(1f, 1f, new Vector2f(300, 300), "Overlay 3", new Font("11613.ttf"), Button3Click);
            layer.Add(Button);
            layer.Add(Button2);
            layer.Add(Button3);
        }

        public override void Update(float DeltaTime)
        {
            base.Update(DeltaTime);

            t = 50f * DeltaTime;

            layer.SetPosition(new Vector2f(layer.Position.X + t, layer.Position.Y));
            layer.Update();
        }

        public override void Draw()
        {
            GameWindow.Current.Viewport.SetUIView();
            layer.Draw();
            base.Draw();
        }

        private void ButtonClick()
        {
                ShowOverlay(TestOverlay);
        }

        private void Button2Click()
        {
            ShowOverlay(TestOverlay2);
        }

        private void Button3Click()
        {
            ShowOverlay(TestOverlay3);
        }
    }
}
