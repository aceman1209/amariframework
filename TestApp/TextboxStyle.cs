﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Amari.UI;
using SFML.Graphics;
using SFML.System;

namespace TestApp
{
    class TextboxStyle : Style
    {
        //public override Sprite Background { get; protected set; }
        //public override float BackgroundFrameSize { get; protected set; }
        public override Color Active { get; protected set; }
        public override Color Inactive { get; protected set; }
        public override Color Disabled { get; protected set; }
        public override Character CharacterInfo { get; protected set; }
        public override Font Font { get; protected set; }

        public TextboxStyle()
        {
            //Background = new Sprite(new Texture("text-box-png-25.png"));
            //BackgroundFrameSize = 40f;
            Active = new Color(82, 132, 239);
            Inactive = new Color(52, 152, 219);
            Disabled = new Color(52, 152, 219, 100);
            CharacterInfo = new Character(24);
            Font = new Font("11613.ttf");
        }
    }
}
