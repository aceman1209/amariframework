﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SFML.Graphics;
using SFML.System;
using SFML.Window;
using Amari;
using Amari.Screens;

namespace TestApp
{
    class Program
    {
        public static TestScreen1 testScreen1;

        [STAThread]
        static void Main(string[] args)
        {
            testScreen1 = new TestScreen1();

            Core.Screens.Add(testScreen1);
            Core.InitializeWindow(1280, 720, "Amari Test");
            Core.Run();
        }
    }
}
