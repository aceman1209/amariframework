﻿using SFML.System;

namespace Amari.Maths
{
    public class Algebra
    {
        public static Vector2f Lerp(Vector2f Start, Vector2f End, float t)
        {
            float X = Lerp(Start.X, End.X, t);
            float Y = Lerp(Start.Y, End.Y, t);
            return new Vector2f(X, Y);
        }

        public static float Lerp(float Start, float End, float t)
        {
            return Start + (End - Start) * t;
        }

    }
}
