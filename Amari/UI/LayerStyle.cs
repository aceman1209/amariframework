﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SFML.Graphics;
using SFML.System;

namespace Amari.UI
{
    public abstract class LayerStyle
    {
        public virtual Sprite[] FrameAngle { get; private set; } = new Sprite[4];
        public virtual Sprite[] FrameBorder { get; private set; } = new Sprite[4];

        public virtual Sprite FillImage { get; protected set; }
        public virtual Color FillColor { get; protected set; }
    }
}
