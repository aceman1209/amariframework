﻿using System;
using System.Windows.Forms;
using SFML.System;
using SFML.Graphics;
using SFML.Window;

namespace Amari.UI
{
    public class TextBox : Widget, Drawable
    {
        Vector2f Position;

        RectangleShape Box;

        Style Style;
        SpriteStyle SpriteStyle;

        Text Label;
        Text Text;

        public bool IsActive { get; private set; }
        public bool IsDisabled;

        Vector2f Scale;
        private uint CharacterSize;

        bool MaskedText;
        char TextMask;

        Color Active, Inactive, Disabled;

        string StringContainer;

        public TextBox(string Label, Vector2f Position, Vector2f Size, Style Style, bool MaskedText = false, char PassowrdChar = ' ')
        {
            this.Position = Position;
            Active = new Color(82, 132, 239);
            Inactive = new Color(52, 152, 219);
            Disabled = new Color(52, 152, 219, 100);

            Box = new RectangleShape();
            Box.Position = Position;
            Box.Size = Size;
            Box.FillColor = Inactive;
            //Box.Origin = new Vector2f(Box.Origin.X, Box.GetLocalBounds().Height / 2);
            this.Label = new Text(Label, Style.Font, CharacterSize);
            this.Label.Origin = new Vector2f(this.Label.Origin.X, this.Label.Origin.Y / 2);
            Text = new Text("", Style.Font, CharacterSize);

            Text.Position = Position;

            StringContainer = "";

            this.MaskedText = MaskedText;
            TextMask = PassowrdChar;

            Console.WriteLine($"{Box.GetLocalBounds().Height}");
            this.Label.Position = new Vector2f(Position.X, Position.Y + Box.GetLocalBounds().Height / 4f);
        }

        public TextBox(string Label, Vector2f Position, Vector2f Scale, SpriteStyle Style, bool MaskedText = false, char Mask = ' ')
        {
            this.Style = Style;
            //this.Position = Position + new Vector2f(Style.BackgroundFrameSize * Scale.X, 0);
            this.Scale = Scale;

            StringContainer = "";

            //Style.Background.Position = Position;

            //Style.Background.Scale = Scale;

            Active = Style.Active;
            Inactive = Style.Inactive;
            Disabled = Style.Disabled;

            //Style.Background.Position = Position;

            this.Label = new Text(Label, Style.Font, Style.CharacterInfo.Size);
            this.Label.Origin = new Vector2f(0, Style.CharacterInfo.Size / 2);
            //this.Label.Position = new Vector2f(this.Position.X, this.Position.Y + (Style.Background.GetLocalBounds().Height / 2f) * Scale.Y);

            Console.WriteLine($"{this.Label.Origin}");


            Text = new Text(StringContainer, Style.Font, Style.CharacterInfo.Size);
            Text.Origin = this.Label.Origin;
            //Text.Position = new Vector2f(this.Position.X, this.Position.Y + (Style.Background.GetLocalBounds().Height / 2f) * Scale.Y);

            this.MaskedText = MaskedText;
            TextMask = Mask;

            //Core.MainGameWindow.TextEntered += RenderWindow_TextBox_TextEntered;
            TextBoxsList.Add(this);
            
        }

        public override void Draw(RenderTarget target, RenderStates states)
        {
            /*if (!IsActive)
                //Style.Background.Color = Inactive;
            else
                //Style.Background.Color = Active;

            if (IsDisabled)
                //Style.Background.Color = Disabled;

            //target.Draw(Style.Background, states);

            if (StringContainer.Length == 0)
                target.Draw(Label, states);
            else
                target.Draw(Text, states);*/

        }

        public override void Handle()
        {
            if (!IsDisabled)
            {
                //Vector2f MousePosition = Core.MainMouse.GetPositionInView(Core.MainViewport.HudView);

               /* if (Style.Background.GetGlobalBounds().Contains(MousePosition.X, MousePosition.Y))
                {
                    if (Core.MainMouse.IsButtonClicked(Mouse.Button.Left))
                    {
                        IsActive = true;
                        Core.MainKeyboard.InputUIHandled = true;
                        Core.MainGameWindow.SetKeyRepeatEnabled(true);
                    }
                }
                else
                {
                    if (Core.MainMouse.IsButtonClicked(Mouse.Button.Left))
                    {
                        IsActive = false;
                    }
                    if (!IsAnyTextBoxIsActive())
                    {
                        Core.MainKeyboard.InputUIHandled = false;
                        Core.MainGameWindow.SetKeyRepeatEnabled(false);
                    }
                }*/
            }
        }

        public string GetInput()
        {
            return StringContainer;
        }

        private void CalculateString()
        {
            Text.DisplayedString = StringContainer;

            if (MaskedText)
            {
                Text.DisplayedString = "";
                for (int i = 0; i < StringContainer.Length; i++)
                {
                    Text.DisplayedString += TextMask;
                }
            }

           /* while (Text.GetLocalBounds().Width + Style.BackgroundFrameSize * Scale.X > ((Style.Background.GetLocalBounds().Width * Scale.X) - Style.BackgroundFrameSize * Scale.X))
            {
                Text.DisplayedString = Text.DisplayedString.Remove(0, 1);
            }
            Console.WriteLine($"{Style.Background.GetLocalBounds().Width} {Style.Background.GetLocalBounds().Width - Style.BackgroundFrameSize}");*/
        }

        private void RenderWindow_TextBox_TextEntered(object sender, TextEventArgs e)
        {
            if (IsActive && !IsDisabled)
           {
                if (e.Unicode == "\b")
                {
                    if (StringContainer.Length > 0)
                    {
                        StringContainer = StringContainer.Remove(StringContainer.Length - 1);
                        CalculateString();
                    }
                }
                else if (e.Unicode == "\u0018")
                {
                    if (!MaskedText)
                    {
                        Clipboard.SetText(StringContainer);
                        StringContainer = "";
                        CalculateString();
                    }
                }
                else if (e.Unicode == "\u0003")
                {
                    if (!MaskedText)
                        Clipboard.SetText(StringContainer);
                }
                else if (e.Unicode == "\u0016")
                {
                    if (Clipboard.ContainsText())
                    {
                        StringContainer += Clipboard.GetText();
                        CalculateString();
                        Console.WriteLine($"{StringContainer}");
                    }
                }
                else
                {
                    StringContainer += e.Unicode;
                    CalculateString();
                }
                
            }
        }
    }
}
