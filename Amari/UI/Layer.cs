﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Amari.Display;
using SFML.System;
using SFML.Graphics;

namespace Amari.UI
{
    public class Layer
    {
        LayerStyle Style;
        RectangleShape LayerBox;
        List<Widget> WidgetsList;

        private bool NeedPositionUpdate = false;

        public Layer(LayerStyle Style, Vector2f Position, Vector2f Size)
        {
            this.Style = Style;
            WidgetsList = new List<Widget>();

            LayerBox = new RectangleShape(Size);
            LayerBox.Position = Position;
            LayerBox.FillColor = Style.FillColor;

            if (Style.FrameBorder != null)
            {

            }

            if (Style.FrameAngle != null)
            { }


            NeedPositionUpdate = true;
        }

        public void Add(Widget widget)
        {
            foreach (var Widget in WidgetsList)
            {
                if (Widget == widget) return;
            }
            widget.Position = GetGlobalPosition(widget.LayerPositionX, widget.LayerPositionY);
            WidgetsList.Add(widget);
        }

        public void Remove(Widget widget)
        {
            WidgetsList.Remove(widget);
        }

        public void Add(Widget[] widget)
        {
            for (int i = 0; i < widget.Length; i++)
            {
                Add(widget[i]);
            }
        }

        public void Remove(Widget[] widget)
        {
            for (int i = 0; i < widget.Length; i++)
            {
                Remove(widget[i]);
            }
        }

        public void Update()
        {

            for (int i = 0; i < WidgetsList.Count; i++)
            {
                if (NeedPositionUpdate)
                    WidgetsList[i].Position = GetGlobalPosition(WidgetsList[i].LayerPositionX, WidgetsList[i].LayerPositionY);
                WidgetsList[i].Handle();
            }

            if (NeedPositionUpdate) NeedPositionUpdate = false;
        }

        public void Draw()
        {
            GameWindow.Current.Draw(LayerBox);
            for (int i = 0; i < WidgetsList.Count; i++)
            {
                GameWindow.Current.Draw(WidgetsList[i]);
            }
        }

        public void SetPosition(Vector2f Position)
        {
            LayerBox.Position = Position;
            NeedPositionUpdate = true;
        }

        private Vector2f GetGlobalPosition(float LayerPositionX, float LayerPositionY)
        {
            float x = LayerBox.GetGlobalBounds().Left + LayerBox.Size.X * LayerPositionX;
            float y = LayerBox.GetGlobalBounds().Top + LayerBox.Size.Y * LayerPositionX;
            return new Vector2f(x, y);
        }
    }

    public struct LayerInfo
    {
        public Layer ParentLayer { get; private set; }
        public float LayerPositionX { get; private set; }
        public float LayerPositionY { get; private set; }

        public LayerInfo(Layer ParentLayer, Vector2f PositionOnLayer)
        {
            this.ParentLayer = ParentLayer;
            LayerPositionX = PositionOnLayer.X;
            LayerPositionY = PositionOnLayer.Y;
        }
    }
}
