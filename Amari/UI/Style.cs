﻿using SFML.Graphics;
using SFML.System;

namespace Amari.UI
{
    public abstract class Style
    {
        public abstract Color Active { get; protected set; }
        public abstract Color Inactive { get; protected set; }
        public abstract Color Disabled { get; protected set; }

        public abstract Character CharacterInfo { get; protected set; }

        public abstract Font Font { get; protected set; }
    }

    public struct Character
    {
        public uint Size { get; private set; }
        public bool AutoSize { get; private set; }

        public Character(uint Size, bool AutoSize = false)
        {
            this.Size = Size;
            this.AutoSize = AutoSize;
        }
    }
}
