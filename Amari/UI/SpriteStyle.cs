﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SFML.Graphics;
using SFML.System;

namespace Amari.UI
{
    public abstract class SpriteStyle : Style
    {
        public abstract Sprite Background { get; protected set; }
        public abstract float BackgroundFrameSize { get; protected set; }
    }
}
