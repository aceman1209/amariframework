﻿using SFML.Graphics;
using SFML.System;
using SFML.Window;
using Amari.Display;
using Amari.Input;
using System;

namespace Amari.UI
{
    public class Button : Widget, Drawable
    {

        Vector2f Size;
        Text DisplayedString;

        Action OnClick;

        public Button(Vector2f Position, Vector2f Size, string DisplayedString, Font font, Action Click)
        {
            this.Position = Position;
            this.Size = Size;
            this.DisplayedString = new Text(DisplayedString, font)
            {
                Position = Position
            };
            OnClick = Click;
        }

        public Button(Vector2f PositionOnLayer, Vector2f Size, string DisplayedString, Font font, Action Click)
        {
            this.Size = Size;
            this.DisplayedString = new Text(DisplayedString, font)
            {
                Position = Position,
            };
            this.DisplayedString.Origin = new Vector2f(this.DisplayedString.GetLocalBounds().Width / 2, this.DisplayedString.GetLocalBounds().Height / 2);
            OnClick = Click;
        }

        public override void Handle()
        {
            UpdatePosition();
            Vector2f MousePosition = new Vector2f(MouseController.Current.ViewMousePositionX, MouseController.Current.ViewMousePositionY);
            if (DisplayedString.GetGlobalBounds().Contains(MousePosition.X, MousePosition.Y))
            {
                if (MouseController.Current.IsButtonClicked(Mouse.Button.Left))
                {
                    OnClick();
                }
            }
        }

        public override void Draw(RenderTarget target, RenderStates states)
        {
            target.Draw(DisplayedString);
        }

        private void UpdatePosition()
        {
            if (DisplayedString.Position != Position)
                DisplayedString.Position = Position;
        }
    }
}
