﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SFML.System;
using SFML.Graphics;
using Amari;

namespace Amari.UI
{
    public enum Type
    {
        OnLayer,
        Free
    }

    public abstract class Widget : Drawable
    {
        protected static List<TextBox> TextBoxsList { get; private set; } = new List<TextBox>();
        public bool NeedPositionUpdate { get; protected set; }

        public Vector2f Position { get; set; }
        public LayerInfo LayerInfo { get; protected set; }


        protected virtual bool IsAnyTextBoxIsActive()
        {
            foreach (var TextBox in TextBoxsList)
            {
                if (TextBox.IsActive) return true;
            }
            return false;
        }

        public virtual void UpdateLayerPosition()
        {
        }

        public abstract void Handle();
        public abstract void Draw(RenderTarget target, RenderStates states);
        
    }
}
