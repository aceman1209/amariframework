﻿

using System;
using System.Collections.Generic;

using Amari.Exception;
using Amari.Screens;
using Amari.Logging;
using Amari.Display;
using KeyboardController = Amari.Input.KeyboardController;
using MouseController = Amari.Input.MouseController;

using SFML.Graphics;
using SFML.Window;
using SFML.System;

using KeyboardSFML = SFML.Window.Keyboard;
using MouseSFML = SFML.Window.Mouse;
using ViewSFML = SFML.Graphics.View;

namespace Amari
{
    public static class Core
    {
        //Главное окно
        public static GameWindow GameWindow { get; private set; }

        //Экраны
        public static List<Screen> Screens { get; private set; } = new List<Screen>();

        //Управление
        public static KeyboardController KeyboardController { get; private set; }
        public static MouseController MouseController { get; private set; }

        //дебаг-переменные
        static public bool IsDebug { get; private set; } = true;

        static Clock Timer = new Clock();

        //Delta Clock
        static Clock DeltaClock = new Clock();
        public static void InitializeWindow(uint Width, uint Height, string Title)
        {
            GameWindow = new GameWindow(1280, 720);

            Timer.Restart();
            MouseController = new MouseController();
        }


        public static void Run()
        {
            if (GameWindow == null)
                InitializeWindow(1280, 720, "Application");

            while (GameWindow.IsOpen)
            {
                float DeltaTime = DeltaClock.ElapsedTime.AsSeconds();
                DeltaClock.Restart();
                //Console.WriteLine($"{GameWindow.Title}");
                GameWindow.DispatchEvents();

                GameWindow.Clear(Color.Cyan);

                    foreach (var screen in Screens)
                    {
                        if (screen.IsCurrentScreen)
                        {
                            screen.Update(DeltaTime);
                            screen.Draw();
                        }
                    }

                MouseController.UpdateInput();

                GameWindow.Viewport.Draw();
                   GameWindow.Display();
            }
        }
    }
}
