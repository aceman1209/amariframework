﻿using System;
using SFML.Window;
using SFML.System;

using SFMLKeyboard = SFML.Window.Keyboard;

namespace Amari.Input
{
    public class Keyboard
    {
        public bool InputUIHandled { get; set; }

        //Нажата ли кнопка
        private bool[] is_Button_Pressed = new bool[(int)SFMLKeyboard.Key.KeyCount];
        private bool[] is_Button_Clicked = new bool[(int)SFMLKeyboard.Key.KeyCount];
        private bool[] is_Button_Released = new bool[(int)SFMLKeyboard.Key.KeyCount];

        Clock Timer;

        //Конструктор класса
        public Keyboard(bool OnFocusHandle)
        {
            //Core.MainGameWindow.KeyPressed += RenderWindow_KeyPressed;
            //Core.MainGameWindow.KeyReleased += RenderWindow_KeyReleased;
            Timer = new Clock();
        }

        public void UpdateInput()
        {
            Timer.Restart();
            for (int i = 0; i < is_Button_Pressed.Length; i++)
            {
                if (is_Button_Pressed[i])
                    is_Button_Clicked[i] = true;
                else
                    is_Button_Clicked[i] = false;
            }
            //Console.WriteLine($"Keyboard Input Update: {Timer.ElapsedTime.AsMicroseconds()}");
        }

        //Проверяет, нажата ли кнопка постоянно
        public bool IsButtonPressed(SFMLKeyboard.Key key)
        {
            if (!InputUIHandled)
                return is_Button_Pressed[(int)key];
            else
                return false;
        }

        //Проверяет, нажата ли кнопка один раз
        public bool IsButtonClicked(SFMLKeyboard.Key key)
        {
            if ((is_Button_Pressed[(int)key] && !is_Button_Clicked[(int)key]) && !InputUIHandled)
                return true;
            else
                return false;
        }

        private void RenderWindow_KeyReleased(object sender, KeyEventArgs e)
        {
            if (e.Code != SFMLKeyboard.Key.Unknown)
            {
                is_Button_Pressed[(int)e.Code] = true;
                is_Button_Released[(int)e.Code] = false;
            }
        }

        private void RenderWindow_KeyPressed(object sender, KeyEventArgs e)
        {
            if (e.Code != SFMLKeyboard.Key.Unknown)
            { 
                is_Button_Pressed[(int)e.Code] = false;
                is_Button_Released[(int)e.Code] = true;
            }
        }
    }
}
