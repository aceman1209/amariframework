﻿using Amari.Display;
using SFML.Window;
using SFML.Graphics;
using SFML.System;

using System;

using SFMLMouse = SFML.Window.Mouse;

namespace Amari.Input
{
    public class MouseController
    {
        public static MouseController Current { get; private set; }

        //Позиция мыши и дельта колеса мыши
        public int MousePositionX { get; private set; }
        public int MousePositionY { get; private set; }
        public float ViewMousePositionX { get; private set; }
        public float ViewMousePositionY { get; private set; }
        public float WheelDelta { get; private set; }

        //Нажата ли кнопка
        private bool[] is_Button_Pressed = new bool[(int)SFMLMouse.Button.ButtonCount];
        private bool[] is_Button_Clicked = new bool[(int)SFMLMouse.Button.ButtonCount];
        private bool[] is_Button_Released = new bool[(int)SFMLMouse.Button.ButtonCount];

        //Конструктор класса
        public MouseController()
        {
            GameWindow.Current.MouseWheelMoved += GameWindow_MouseWheelMoved;
            GameWindow.Current.MouseMoved += GameWindow_MouseMoved;

            GameWindow.Current.MouseButtonPressed += GameWindow_MouseButtonPressed;
            GameWindow.Current.MouseButtonReleased += GameWindow_MouseButtonReleased;

            Current = this;
        }

        //Обновление ввода
        public void UpdateInput()
        {
            for (int i = 0; i < is_Button_Pressed.Length; i++)
            {
                if (is_Button_Pressed[i])
                    is_Button_Clicked[i] = true;
                else
                    is_Button_Clicked[i] = false;
            }
            Vector2f MapPixelCoords = GameWindow.Current.MapPixelToCoords(new Vector2i(MousePositionX, MousePositionY));
            ViewMousePositionX = MapPixelCoords.X;
            ViewMousePositionY = MapPixelCoords.Y;
            ResetWheelDelta();
        }

        //Проверяет, нажата ли кнопка постоянно
        public bool IsButtonPressed(SFMLMouse.Button button)
        {
            return is_Button_Pressed[(int)button];
        }

        //Проверяет, нажата ли кнопка один раз
        public bool IsButtonClicked(SFMLMouse.Button button)
        {
            if (is_Button_Pressed[(int)button] && !is_Button_Clicked[(int)button])
                return true;
            else
                return false;
        }


        //Устанавливает, виден ли курсор в окне
        public void SetCursorVisible(bool Visible) => GameWindow.Current.SetCursorVisible(Visible);

        //Восстанавливает дельту колеса мыши
        private void ResetWheelDelta()
        {
            WheelDelta = 0f;
        }

        //Ивент перемещения мыши
        private void GameWindow_MouseMoved(object sender, MouseMoveEventArgs e)
        {
            MousePositionX = e.X;
            MousePositionY = e.Y;
        }

        //Ивент колеса мыши
        private void GameWindow_MouseWheelMoved(object sender, MouseWheelEventArgs e)
        {
            WheelDelta = e.Delta;
        }

        //Ивенты кнопок
        private void GameWindow_MouseButtonPressed(object sender, MouseButtonEventArgs e)
        {
            is_Button_Pressed[(int)e.Button] = true;
            is_Button_Released[(int)e.Button] = false;
        }

        private void GameWindow_MouseButtonReleased(object sender, MouseButtonEventArgs e)
        {
            is_Button_Pressed[(int)e.Button] = false;
            is_Button_Released[(int)e.Button] = true;    
        }
    }
}
