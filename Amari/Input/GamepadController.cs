﻿using System;
using SFML.Window;
using Amari.Display;

namespace Amari.Input
{
    public class GamepadController
    {
        public bool InputUIHandled { get; set; }
        //Перечисления
        public enum Buttons
        {
            A,
            B,
            X,
            Y,
            LB,
            RB,
            BACK,
            START,
            LJoyButton,
            RJoyButton
        }
        public enum Joys
        {
            Right,
            Left
        }
        public enum Triggers
        {
            Right,
            Left
        }

        //Структуры
        struct Joy
        {
            public float AxisPositionX { get; set; }
            public float AxisPositionY { get; set; }
            public Joystick.Axis AxisX { get; set; }
            public Joystick.Axis AxisY { get; set; }
        }
        struct Trigger
        {
            public float StrangeL { get; set; }
            public float StrangeR { get; set; }
            public Joystick.Axis Axis { get; set; }
        }

        //Контроллеры
        Joy RJoy, LJoy, CrossJoy;
        Trigger RLTrigger;

        //Информация о геймпаде
        Joystick.Identification info;
        public uint GamepadID { get; private set; }

        //Нажата ли кнопка
        private bool[] is_Button_Pressed = new bool[Enum.GetNames(typeof(Buttons)).Length];
        private bool[] is_Button_Clicked = new bool[Enum.GetNames(typeof(Buttons)).Length];

        //Мертвая зона
        private int Axis_DeadZone = 30;
        private int Trigger_DeadZone = 5;

        //Конструктор класса
        public GamepadController(uint ID)
        {
            GamepadID = ID;
            Init();
        }

        //Возвращает, нажата ли кнопка постоянно
        public bool IsButtonPressed(Buttons button)
        {
            return is_Button_Pressed[(int)button];
        }
        //Возвращает, нажата ли кнопка один раз
        public bool IsButtonPressed(Buttons button, bool SingleTap)
        {
            if ((is_Button_Pressed[(int)button] && !is_Button_Clicked[(int)button]) && !InputUIHandled)
                return true;
            else
                return false;
        }

        //Возвращает имя 
        public string GetGamepadName()
        {
            return info.Name;
        }

        //Проверяет подключение
        public bool isConnected()
        {
            return Joystick.IsConnected(GamepadID);
        }

        //Возвращает позицию джостика по X
        public float GetJoyPositionX(Joys joy)
        {
            switch (joy)
            {
                case Joys.Right:
                    return RJoy.AxisPositionX;
                case Joys.Left:
                    return LJoy.AxisPositionX;
                default:
                    return 0;
            }
        }

        //Возвращает позицию джостика по Y
        public float GetJoyPositionY(Joys joy)
        {
            switch (joy)
            {
                case Joys.Right:
                    return RJoy.AxisPositionY;
                case Joys.Left:
                    return LJoy.AxisPositionY;
                default:
                    return 0;
            }
        }

        //Возвращает силу нажатия на триггер
        public float GetTriggerStrange(Triggers trigger)
        {
            switch (trigger)
            {
                case Triggers.Right:
                    return RLTrigger.StrangeR;
                case Triggers.Left:
                    return RLTrigger.StrangeL;
                default:
                    return 0;
            }
        }

        //Инициализация
        private void Init()
        {
            Joystick.Update();
            info = Joystick.GetIdentification(GamepadID);

            //Ивенты окна
            GameWindow.Current.JoystickMoved += RenderWindow_JoystickMoved;

            //Правый джостик, назначение осей
            RJoy.AxisX = Joystick.Axis.U;
            RJoy.AxisY = Joystick.Axis.R;

            //Левый джостик, назначение осей
            LJoy.AxisX = Joystick.Axis.X;
            LJoy.AxisY = Joystick.Axis.Y;

            //Триггеры, назначение оси
            RLTrigger.Axis = Joystick.Axis.Z;

            //Крестовина, назначение осей
            CrossJoy.AxisX = Joystick.Axis.PovX;
            CrossJoy.AxisY = Joystick.Axis.PovY;
        }

        //Рассчет мертвой зоны
        private float CalculateMagnitude(float Axis_X, float Axis_Y)
        {
            return (float)Math.Sqrt(Axis_X * Axis_X + Axis_Y * Axis_Y);
        }

        //Ивент джостиков, крестовины и триггеров
        private void RenderWindow_JoystickMoved(object sender, JoystickMoveEventArgs e)
        {
            if (InputUIHandled)
            {
                Joystick.Update();
                //Left Joy
                if (e.Axis == LJoy.AxisX || e.Axis == LJoy.AxisY)
                {
                    if (CalculateMagnitude(Joystick.GetAxisPosition(GamepadID, LJoy.AxisX), Joystick.GetAxisPosition(GamepadID, LJoy.AxisY)) > Axis_DeadZone)
                    {
                        LJoy.AxisPositionX = Joystick.GetAxisPosition(GamepadID, LJoy.AxisX);
                        LJoy.AxisPositionY = Joystick.GetAxisPosition(GamepadID, LJoy.AxisY);
                        Console.WriteLine($"LEFT JOY {LJoy.AxisPositionX} {LJoy.AxisPositionY} ");
                    }
                    else
                    {
                        LJoy.AxisPositionX = 0f;
                        LJoy.AxisPositionY = 0f;
                    }
                }

                //Right Joy
                if (e.Axis == RJoy.AxisX || e.Axis == RJoy.AxisY)
                {
                    if (CalculateMagnitude(Joystick.GetAxisPosition(GamepadID, RJoy.AxisX), Joystick.GetAxisPosition(GamepadID, RJoy.AxisY)) > Axis_DeadZone)
                    {
                        RJoy.AxisPositionX = Joystick.GetAxisPosition(GamepadID, RJoy.AxisX);
                        RJoy.AxisPositionY = Joystick.GetAxisPosition(GamepadID, RJoy.AxisY);
                        Console.WriteLine($"Right JOY {RJoy.AxisPositionX} {RJoy.AxisPositionY} ");
                    }
                    else
                    {
                        RJoy.AxisPositionX = 0f;
                        RJoy.AxisPositionY = 0f;
                    }
                }

                //Cross Joy
                if (e.Axis == CrossJoy.AxisX || e.Axis == CrossJoy.AxisY)
                {
                    if (CalculateMagnitude(Joystick.GetAxisPosition(GamepadID, CrossJoy.AxisX), Joystick.GetAxisPosition(GamepadID, CrossJoy.AxisX)) > Axis_DeadZone)
                    {
                        CrossJoy.AxisPositionX = Joystick.GetAxisPosition(GamepadID, CrossJoy.AxisX);
                        CrossJoy.AxisPositionY = Joystick.GetAxisPosition(GamepadID, CrossJoy.AxisY);

                        Console.WriteLine($"Cross Joy {CrossJoy.AxisPositionX}   {CrossJoy.AxisPositionY}");
                    }
                    else
                    {
                        CrossJoy.AxisPositionX = 0;
                        CrossJoy.AxisPositionY = 0;
                    }
                }

                //Triggers
                if (e.Axis == RLTrigger.Axis)
                {
                    if (CalculateMagnitude(Joystick.GetAxisPosition(GamepadID, RLTrigger.Axis), Joystick.GetAxisPosition(GamepadID, RLTrigger.Axis)) > Trigger_DeadZone)
                    {
                        if (Joystick.GetAxisPosition(GamepadID, RLTrigger.Axis) > 0)
                            RLTrigger.StrangeL = Joystick.GetAxisPosition(GamepadID, RLTrigger.Axis);
                        if (Joystick.GetAxisPosition(GamepadID, RLTrigger.Axis) < 0)
                            RLTrigger.StrangeR = Joystick.GetAxisPosition(GamepadID, RLTrigger.Axis);
                        Console.WriteLine($"Triggers L {RLTrigger.StrangeL}  R {RLTrigger.StrangeR}");
                    }
                    else
                    {
                        RLTrigger.StrangeL = 0;
                        RLTrigger.StrangeR = 0;
                    }
                }
            }
        }
    }
}
