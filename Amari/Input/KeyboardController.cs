﻿using SFML.Window;
using Amari.Display;

namespace Amari.Input
{
    public class KeyboardController
    {
        public bool InputUIHandled { get; set; }

        //Нажата ли кнопка
        private bool[] is_Button_Pressed = new bool[(int)Keyboard.Key.KeyCount];
        private bool[] is_Button_Clicked = new bool[(int)Keyboard.Key.KeyCount];
        private bool[] is_Button_Released = new bool[(int)Keyboard.Key.KeyCount];


        //Конструктор класса
        public KeyboardController(bool OnFocusHandle)
        {
            GameWindow.Current.KeyPressed += GameWindow_KeyPressed;
            GameWindow.Current.KeyReleased += GameWindow_KeyReleased;
        }

        public void UpdateInput()
        {
            for (int i = 0; i < is_Button_Pressed.Length; i++)
            {
                if (is_Button_Pressed[i])
                    is_Button_Clicked[i] = true;
                else
                    is_Button_Clicked[i] = false;
            }
        }

        //Проверяет, нажата ли кнопка постоянно
        public bool IsButtonPressed(Keyboard.Key key)
        {
            if (!InputUIHandled)
                return is_Button_Pressed[(int)key];
            else
                return false;
        }

        //Проверяет, нажата ли кнопка один раз
        public bool IsButtonClicked(Keyboard.Key key)
        {
            if ((is_Button_Pressed[(int)key] && !is_Button_Clicked[(int)key]) && !InputUIHandled)
                return true;
            else
                return false;
        }

        private void GameWindow_KeyReleased(object sender, KeyEventArgs e)
        {
            if (e.Code != Keyboard.Key.Unknown)
            {
                is_Button_Pressed[(int)e.Code] = true;
                is_Button_Released[(int)e.Code] = false;
            }
        }

        private void GameWindow_KeyPressed(object sender, KeyEventArgs e)
        {
            if (e.Code != Keyboard.Key.Unknown)
            { 
                is_Button_Pressed[(int)e.Code] = false;
                is_Button_Released[(int)e.Code] = true;
            }
        }
    }
}
