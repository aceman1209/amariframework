﻿using System;
using System.IO;
using System.Reflection;

namespace Amari.Logging
{
    public static class Logger
    {
        private static string Name = Assembly.GetExecutingAssembly().FullName.Split(',')[0];
        private static string Path = "Logs\\";
        private static StreamWriter CurrentLog;

        private static bool IsInitialized = Initialize();


        public static void WriteLogLine(string Text)
        {
            if (IsInitialized)
            {
            CurrentLog.WriteLine($"{GenerateDate()} {Name} : {Text}");
            CurrentLog.Flush();
            }
        }

        public static void WriteConsoleLine(string Text)
        {
            Console.WriteLine($"{GenerateDate()} {Name} : {Text}");
        }

        public static void CloseFile()
        {
            CurrentLog.Close();
        }

        private static bool Initialize()
        {
            try
            {
                if (!Directory.Exists(Path)) Directory.CreateDirectory(Path);
                CurrentLog = new StreamWriter(Path + GenerateFilename());
                return true;
            }
            catch
            {
                WriteConsoleLine("Logger initialization failed");
                return false;
            }
        }

        private static string GenerateFilename()
        {
            return string.Format("log_{0:dd.MM.yyyy}_{0:HH.mm.ss}.log", DateTime.Now);
        }

        private static string GenerateDate()
        {
            return string.Format("{0:dd.MM.yyyy} {0:HH:mm:ss.fff}", DateTime.Now);
        }
    }
}
