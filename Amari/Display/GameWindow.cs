﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SFML.Window;
using SFML.Graphics;
using SFML.System;
using System.Reflection;

namespace Amari.Display
{
    public class GameWindow
    {
        public static GameWindow Current { get; private set; }

        private RenderWindow renderWindow;
        private string _Title = "";
        private const int FullscreenNativeResolutionIndex = 0;

        public Viewport Viewport;

        public View DefaultView { get; private set; }
        public bool IsFullscreen { get; private set; }
        public string Title { get { return _Title; } set { renderWindow.SetTitle(value); _Title = value; } }
        public bool IsOpen { get { return renderWindow.IsOpen; } }

        #region Events List

        #region Window Events
        /// <summary>
        /// Event handler for the Closed event
        /// </summary>
        public event EventHandler Closed = null;

        /// <summary>
        /// Event handler for the Resized event
        /// </summary>
        public event EventHandler<SizeEventArgs> Resized = null;

        /// <summary>
        /// Event handler for the LostFocus event
        /// </summary>
        public event EventHandler LostFocus = null;

        /// <summary>
        /// Event handler for the GainedFocus event
        /// </summary>
        public event EventHandler GainedFocus = null;

        public event EventHandler VideoModeChanged = null;
        public event EventHandler VideoModeIsInvalid = null;
        #endregion

        #region Input Events
        /// <summary>
        /// Event handler for the TextEntered event
        /// </summary>
        public event EventHandler<TextEventArgs> TextEntered = null;

        /// <summary>
        /// Event handler for the KeyPressed event
        /// </summary>
        public event EventHandler<KeyEventArgs> KeyPressed = null;

        /// <summary>
        /// Event handler for the KeyReleased event
        /// </summary>
        public event EventHandler<KeyEventArgs> KeyReleased = null;

        /// <summary>
        /// Event handler for the MouseWheelMoved event
        /// </summary>
        public event EventHandler<MouseWheelEventArgs> MouseWheelMoved = null;

        /// <summary>
        /// Event handler for the MouseButtonPressed event
        /// </summary>
        public event EventHandler<MouseButtonEventArgs> MouseButtonPressed = null;

        /// <summary>
        /// Event handler for the MouseButtonReleased event
        /// </summary>
        public event EventHandler<MouseButtonEventArgs> MouseButtonReleased = null;

        /// <summary>
        /// Event handler for the MouseMoved event
        /// </summary>
        public event EventHandler<MouseMoveEventArgs> MouseMoved = null;

        /// <summary>
        /// Event handler for the MouseEntered event
        /// </summary>
        public event EventHandler MouseEntered = null;

        /// <summary>
        /// Event handler for the MouseLeft event
        /// </summary>
        public event EventHandler MouseLeft = null;

        /// <summary>
        /// Event handler for the JoystickButtonPressed event
        /// </summary>
        public event EventHandler<JoystickButtonEventArgs> JoystickButtonPressed = null;

        /// <summary>
        /// Event handler for the JoystickButtonReleased event
        /// </summary>
        public event EventHandler<JoystickButtonEventArgs> JoystickButtonReleased = null;

        /// <summary>
        /// Event handler for the JoystickMoved event
        /// </summary>
        public event EventHandler<JoystickMoveEventArgs> JoystickMoved = null;

        /// <summary>
        /// Event handler for the JoystickConnected event
        /// </summary>
        public event EventHandler<JoystickConnectEventArgs> JoystickConnected = null;

        /// <summary>
        /// Event handler for the JoystickDisconnected event
        /// </summary>
        public event EventHandler<JoystickConnectEventArgs> JoystickDisconnected = null;

        /// <summary>
        /// Event handler for the TouchBegan event
        /// </summary>
        public event EventHandler<TouchEventArgs> TouchBegan = null;

        /// <summary>
        /// Event handler for the TouchMoved event
        /// </summary>
        public event EventHandler<TouchEventArgs> TouchMoved = null;

        /// <summary>
        /// Event handler for the TouchEnded event
        /// </summary>
        public event EventHandler<TouchEventArgs> TouchEnded = null;

        /// <summary>
        /// Event handler for the SensorChanged event
        /// </summary>
        public event EventHandler<SensorEventArgs> SensorChanged = null;
        #endregion

        #endregion

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="Witdh">Game window physics width</param>
        /// <param name="Height">Game window physics height</param>
        /// <param name="Title">Game window title (not necessary)</param>
        /// <param name="IsFullscreen">Game window fullscreen mode (not necessary)</param>
        public GameWindow(uint Witdh, uint Height, string Title = "", bool IsFullscreen = false)
        {
            this.IsFullscreen = IsFullscreen;
            VideoMode StartVideoMode = new VideoMode(Witdh, Height);
            if (!StartVideoMode.IsValid())
                StartVideoMode = IsFullscreen ? VideoMode.FullscreenModes[FullscreenNativeResolutionIndex] : VideoMode.DesktopMode;
            InitializeWindow(StartVideoMode);
            this.Title = Title;
            renderWindow.SetVerticalSyncEnabled(true);
        }

        public void UpdateVideoMode(VideoMode VideoMode)
        {
            renderWindow.Close();
            if (!VideoMode.IsValid())
                VideoMode = IsFullscreen ? VideoMode.FullscreenModes[FullscreenNativeResolutionIndex] : VideoMode.DesktopMode;

            InitializeWindow(VideoMode);
            VideoModeChanged?.Invoke(this, new EventArgs());
        }

        public void Clear() => renderWindow.Clear();
        public void Clear(Color Color) => renderWindow.Clear(Color);

        public void DispatchEvents() => renderWindow.DispatchEvents();
        public void Draw(Drawable drawable) => renderWindow.Draw(drawable);
        public void Draw(Drawable drawable, RenderStates states) => renderWindow.Draw(drawable, states);

        public void Display() => renderWindow.Display();

        public void Close() => renderWindow.Close();
        public void SetView(View View) => renderWindow.SetView(View);

        public Vector2f MapPixelToCoords(Vector2i Position) => renderWindow.MapPixelToCoords(Position);
        public void SetCursorVisible(bool Visible) => renderWindow.SetMouseCursorVisible(Visible);

        private void InitializeWindow(VideoMode VideoMode)
        {
            renderWindow = new RenderWindow(VideoMode, _Title, IsFullscreen ? Styles.Fullscreen : Styles.Titlebar);
            DefaultView = renderWindow.DefaultView;
            LinkEvents();
            Current = this;
            Viewport = new Viewport(VideoMode.Width, VideoMode.Height, 1920, 1080);
        }

        #region Events Link
        /// <summary>
        /// Link events to render window
        /// </summary>
        private void LinkEvents()
        {
            renderWindow.Closed += RenderWindow_Closed;
            renderWindow.Resized += RenderWindow_Resized;
            renderWindow.LostFocus += RenderWindow_LostFocus;
            renderWindow.GainedFocus += RenderWindow_GainedFocus;

            renderWindow.TextEntered += RenderWindow_TextEntered;
            renderWindow.KeyPressed += RenderWindow_KeyPressed;
            renderWindow.KeyReleased += RenderWindow_KeyReleased;

            renderWindow.MouseWheelMoved += RenderWindow_MouseWheelMoved;
            renderWindow.MouseButtonPressed += RenderWindow_MouseButtonPressed;
            renderWindow.MouseButtonReleased += RenderWindow_MouseButtonReleased;
            renderWindow.MouseMoved += RenderWindow_MouseMoved;
            renderWindow.MouseEntered += RenderWindow_MouseEntered;
            renderWindow.MouseLeft += RenderWindow_MouseLeft;

            renderWindow.JoystickButtonPressed += RenderWindow_JoystickButtonPressed;
            renderWindow.JoystickButtonReleased += RenderWindow_JoystickButtonReleased;
            renderWindow.JoystickMoved += RenderWindow_JoystickMoved;
            renderWindow.JoystickConnected += RenderWindow_JoystickConnected;
            renderWindow.JoystickDisconnected += RenderWindow_JoystickDisconnected;

            renderWindow.TouchBegan += RenderWindow_TouchBegan;
            renderWindow.TouchMoved += RenderWindow_TouchMoved;
            renderWindow.TouchEnded += RenderWindow_TouchEnded;
            renderWindow.SensorChanged += RenderWindow_SensorChanged;
        }

        private void RenderWindow_Closed(object sender, EventArgs e) => Closed?.Invoke(sender, e);
        private void RenderWindow_Resized(object sender, SizeEventArgs e) => Resized?.Invoke(sender, e);
        private void RenderWindow_LostFocus(object sender, EventArgs e) => LostFocus?.Invoke(sender, e);
        private void RenderWindow_GainedFocus(object sender, EventArgs e) => GainedFocus?.Invoke(sender, e);
        private void RenderWindow_TextEntered(object sender, TextEventArgs e) => TextEntered?.Invoke(sender, e);
        private void RenderWindow_KeyPressed(object sender, KeyEventArgs e) => KeyPressed?.Invoke(sender, e);
        private void RenderWindow_KeyReleased(object sender, KeyEventArgs e) => KeyReleased?.Invoke(sender, e);
        private void RenderWindow_MouseWheelMoved(object sender, MouseWheelEventArgs e) => MouseWheelMoved?.Invoke(sender, e);
        private void RenderWindow_MouseButtonPressed(object sender, MouseButtonEventArgs e) => MouseButtonPressed?.Invoke(sender, e);
        private void RenderWindow_MouseButtonReleased(object sender, MouseButtonEventArgs e) => MouseButtonReleased?.Invoke(sender, e);
        private void RenderWindow_MouseMoved(object sender, MouseMoveEventArgs e) => MouseMoved?.Invoke(sender, e);
        private void RenderWindow_MouseEntered(object sender, EventArgs e) => MouseEntered?.Invoke(sender, e);
        private void RenderWindow_MouseLeft(object sender, EventArgs e) => MouseLeft?.Invoke(sender, e);
        private void RenderWindow_JoystickButtonPressed(object sender, JoystickButtonEventArgs e) => JoystickButtonPressed?.Invoke(sender, e); 
        private void RenderWindow_JoystickButtonReleased(object sender, JoystickButtonEventArgs e) => JoystickButtonReleased?.Invoke(sender, e);
        private void RenderWindow_JoystickMoved(object sender, JoystickMoveEventArgs e) => JoystickMoved?.Invoke(sender, e);
        private void RenderWindow_JoystickConnected(object sender, JoystickConnectEventArgs e) => JoystickConnected?.Invoke(sender, e);
        private void RenderWindow_JoystickDisconnected(object sender, JoystickConnectEventArgs e) => JoystickDisconnected?.Invoke(sender, e);
        private void RenderWindow_TouchBegan(object sender, TouchEventArgs e) => TouchBegan?.Invoke(sender, e);
        private void RenderWindow_TouchMoved(object sender, TouchEventArgs e) => TouchMoved?.Invoke(sender, e);
        private void RenderWindow_TouchEnded(object sender, TouchEventArgs e) => TouchEnded?.Invoke(sender, e);
        private void RenderWindow_SensorChanged(object sender, SensorEventArgs e) => SensorChanged?.Invoke(sender, e);
        #endregion
    }
}
