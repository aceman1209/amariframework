﻿using SFML.Graphics;
using SFML.System;
using ViewSFML = SFML.Graphics.View;

namespace Amari.Display
{
    public class Viewport
    {
        /// <summary>
        /// Returns virtual screen width.
        /// </summary>
        public uint VirtualScreenWidth { get; private set; }
        /// <summary>
        /// Returns virtual screen height.
        /// </summary>
        public uint VirtualScreenHeight { get; private set; }
        private float VirtualRatio;

        /// <summary>
        /// Returns physics screen width.
        /// </summary>
        public uint PhysicsScreenWidth { get; private set; }
        /// <summary>
        /// Returns physics screen height.
        /// </summary>
        public uint PhysicsScreenHeight { get; private set; }
        private float PhysicsRatio { get; set; }

        /// <summary>
        /// Game view. Shown all of game process, like a game world.
        /// </summary>
        private ViewSFML GameView { get; set; }
        /// <summary>
        /// UI view. Used for UI draw.
        /// </summary>
        private ViewSFML UIView { get; set; }
        /// <summary>
        /// Boxes view. Used for black boxes draw. Have physics size.
        /// </summary>
        private ViewSFML  BoxesView { get; set; }

        /// <summary>
        /// A black boxes on sides.
        /// </summary>
        private RectangleShape VerticalBoxUp, VerticalBoxDown, HorizontalBoxLeft, HorizontalBoxRight;


        /// <summary>
        /// Constructor. Physics screen size - game window size in Windows. Virtual screen size - virtual window with fixed size in game window.
        /// </summary>
        /// <param name="PhysicsScreenWidth">Physics screen width. Game window size in Windows.</param>
        /// <param name="PhysicsScreenHeight">Physics screen height.</param>
        /// <param name="VirtualScreenWidth">Virtual screen width.</param>
        /// <param name="VirtualScreenHeight">Virtual screen height.</param>
        public Viewport(uint PhysicsScreenWidth, uint PhysicsScreenHeight ,uint VirtualScreenWidth, uint VirtualScreenHeight)
        {
            this.VirtualScreenWidth = VirtualScreenWidth;
            this.VirtualScreenHeight = VirtualScreenHeight;
            VirtualRatio = (float)VirtualScreenWidth / VirtualScreenHeight;

            this.PhysicsScreenWidth = PhysicsScreenWidth;
            this.PhysicsScreenHeight = PhysicsScreenHeight;
            PhysicsRatio = (float)PhysicsScreenWidth / PhysicsScreenHeight;

            VerticalBoxUp = new RectangleShape();
            VerticalBoxDown = new RectangleShape();
            HorizontalBoxLeft = new RectangleShape();
            HorizontalBoxRight = new RectangleShape();

            VerticalBoxUp.FillColor = Color.Black;
            VerticalBoxDown.FillColor = Color.Black;
            HorizontalBoxLeft.FillColor = Color.Black;
            HorizontalBoxRight.FillColor = Color.Black;

            Initialize();
        }

        /// <summary>
        /// Draw a boxes.
        /// </summary>
        public void Draw()
        {
            GameWindow.Current.SetView(BoxesView);

            GameWindow.Current.Draw(VerticalBoxUp);
            GameWindow.Current.Draw(VerticalBoxDown);
            GameWindow.Current.Draw(HorizontalBoxLeft);
            GameWindow.Current.Draw(HorizontalBoxRight);
        }
        
        /// <summary>
        /// Set Game View on current window. Used for main game draw.
        /// </summary>
        public void SetGameView() => GameWindow.Current.SetView(GameView);
        /// <summary>
        /// Set UI View on current window. Used for UI draw.
        /// </summary>
        public void SetUIView() => GameWindow.Current.SetView(UIView);

        /// <summary>
        /// Initialize a viewport.
        /// </summary>
        private void Initialize()
        {
            BoxesView = GameWindow.Current.DefaultView;

            ResetBoxes();
            if (PhysicsScreenHeight * VirtualRatio > PhysicsScreenWidth)
            {
                uint newHeight = (VirtualScreenWidth * PhysicsScreenHeight) / PhysicsScreenWidth;
                int displayplace = (int)(newHeight - VirtualScreenHeight) / (-2);

                GameView = new ViewSFML(new FloatRect(0, displayplace, VirtualScreenWidth, newHeight));

                CalculateVetricalBoxes();
            }
            else
            {
                uint newWidth = (PhysicsScreenWidth * VirtualScreenHeight) / PhysicsScreenHeight;
                int displayplace = (int)(newWidth - VirtualScreenWidth) / 2;

                GameView = new ViewSFML(new FloatRect(displayplace, 0, newWidth, VirtualScreenHeight));

                CalculateHorizontalBoxes();
            }

            UIView = new ViewSFML(GameView);
        }

        /// <summary>
        /// Calculate vertical boxes if this need.
        /// </summary>
        private void CalculateVetricalBoxes()
        {
            float vertical_padding = PhysicsScreenHeight - PhysicsScreenWidth / VirtualRatio;

            VerticalBoxUp.Size = new Vector2f(BoxesView.Size.X, vertical_padding / 2);
            VerticalBoxDown.Size = new Vector2f(BoxesView.Size.X, vertical_padding / 2);

            VerticalBoxUp.Position = new Vector2f(0, 0);
            VerticalBoxDown.Position = new Vector2f(0, BoxesView.Size.Y - VerticalBoxDown.Size.Y);
        }
        /// <summary>
        /// Calculate horizontal boxes if this need.
        /// </summary>
        private void CalculateHorizontalBoxes()
        {
            float horizontal_padding = PhysicsScreenWidth - PhysicsScreenHeight * VirtualRatio;

            HorizontalBoxLeft.Size = new Vector2f(horizontal_padding / 2, BoxesView.Size.Y);
            HorizontalBoxRight.Size = new Vector2f(horizontal_padding / 2, BoxesView.Size.Y);

            HorizontalBoxLeft.Position = new Vector2f(0, 0);
            HorizontalBoxRight.Position = new Vector2f(BoxesView.Size.X - HorizontalBoxRight.Size.X, 0);
        }
        /// <summary>
        /// Reset boxes for next initialization.
        /// </summary>
        private void ResetBoxes()
        {
            HorizontalBoxLeft.Size = HorizontalBoxRight.Size = VerticalBoxUp.Size = VerticalBoxDown.Size = new Vector2f(0, 0);
        }
    }
}
