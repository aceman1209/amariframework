﻿using System;
using System.Runtime.Serialization;

namespace Amari.Exception
{
    [Serializable]
    public class GameWindowIsNotInitializedException : System.Exception
    {
        public GameWindowIsNotInitializedException() : base("Game window isn't initialized") { }

        public GameWindowIsNotInitializedException(SerializationInfo info, StreamingContext context) : base(info, context) { }

    }

    public class GameVideoModeNotInitializedException : System.Exception
    {
        public GameVideoModeNotInitializedException() : base("Game window videomode doesn't initialized") { }

        public GameVideoModeNotInitializedException(SerializationInfo info, StreamingContext context) : base(info, context) { }

    }
}
