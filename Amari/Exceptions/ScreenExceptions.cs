﻿using System;
using System.Runtime.Serialization;

namespace Amari.Exception
{
    [Serializable]
    public class ScreenNameDoesntUnique : System.Exception
    {   
        ////////////////////////////////////////////////////////////
        /// <summary>
        /// Default constructor
        /// </summary>
        ////////////////////////////////////////////////////////////
        public ScreenNameDoesntUnique() : base("Screens names must be unique") { }

        ////////////////////////////////////////////////////////////
        /// <summary>
        /// Initialize an instance of the exception with serialized data
        /// </summary>
        /// <param name="info">Serialized data</param>
        /// <param name="context">Contextual informations</param>
        ////////////////////////////////////////////////////////////
        public ScreenNameDoesntUnique(SerializationInfo info, StreamingContext context) : base(info, context) { }

    }

    [Serializable]
    public class ScreensListIsEmpty : System.Exception
    {
        ////////////////////////////////////////////////////////////
        /// <summary>
        /// Default constructor
        /// </summary>
        ////////////////////////////////////////////////////////////
        public ScreensListIsEmpty() : base("Screens list is empty") { }

        ////////////////////////////////////////////////////////////
        /// <summary>
        /// Initialize an instance of the exception with serialized data
        /// </summary>
        /// <param name="info">Serialized data</param>
        /// <param name="context">Contextual informations</param>
        ////////////////////////////////////////////////////////////
        public ScreensListIsEmpty(SerializationInfo info, StreamingContext context) : base(info, context) { }

    }
}
