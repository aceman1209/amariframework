﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SFML.Graphics;
using SFML.System;
using Amari.Logging;
using Amari.Display;

namespace Amari.Screens
{
    public abstract class Screen
    {
        private const byte OverlayDefaultAlpha = 100;
        static List<Screen> ScreensList = new List<Screen>();
        public Screen ParentScreen { get; protected set; }
        public string Name { get; protected set; }
        public bool IsCurrentScreen { get; protected set; }

        public List<Overlay> Overlays { get; protected set; } = new List<Overlay>();
        private RectangleShape OverlayFade = new RectangleShape();

        protected virtual void Initialize() => ScreensList.Add(this);

        public virtual void Update(float DeltaTime)
        {
            OverlayFade.FillColor = new Color(0, 0, 0, OverlayDefaultAlpha);
            for (int i = 0; i < Overlays.Count; i++)
            {
                    Overlays[i].Update(DeltaTime);
            }
        }

        public virtual void Draw()
        {
            foreach (var overlay in Overlays)
            {
                    GameWindow.Current.Draw(OverlayFade);
                    overlay.Draw();
            }
        }

        public virtual void Switch(Screen screen)
        {
            if (screen.IsCurrentScreen)
            {
                if (Core.IsDebug)
                    Logger.WriteLogLine("Screen is already active");
                return;
            }

            IsCurrentScreen = false;
            screen.ParentScreen = this;
            screen.IsCurrentScreen = true;
        }

        public virtual void Switch(string Name)
        {
            foreach (var Screen in ScreensList)
            {
                if (Screen.Name.ToLower() == Name.ToLower())
                {
                    Switch(Screen);
                    return;
                }
            }
            if (Core.IsDebug)
                Logger.WriteLogLine("Screen not found");
        }

        public virtual void Return()
        {
            if (ParentScreen == null)
            {
                if (Core.IsDebug)
                    Logger.WriteLogLine("Parent screen is empty");
                return;
            }
            IsCurrentScreen = false;

            ParentScreen.IsCurrentScreen = true;
            ParentScreen = null;
        }

        public virtual void ShowOverlay(Overlay overlay)
        {
            for (int i = 0; i <= Overlays.Count; i++)
            {
                if (i == Overlays.Count)
                    Overlays.Add(overlay);
                if (overlay == Overlays[i]) break;
            }
            overlay.Show(this);
        }
    }
}
