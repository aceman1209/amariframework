﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amari.Screens
{

    /// <summary>
    /// Overlay status enum
    /// </summary>
    public enum OverlayStatus
    {
        None,
        Showing,
        Showed,
        Hidding,
        Hided
    }

    public abstract class Overlay
    {
        // Parent Screen link
        private Screen ParentScreen;
        // Overlay status
        private OverlayStatus Status = OverlayStatus.None;

        /// <summary>
        /// Initializing method.
        /// </summary>
        protected abstract void Initialize();

        /// <summary>
        /// Show animation method. Use base.ShowAnimation(DeltaTime) after animation ends.
        /// </summary>
        /// <param name="DeltaTime">Game delta time</param>
        protected virtual void ShowAnimation(float DeltaTime)
        {
            if (Status == OverlayStatus.Showing)
                Status = OverlayStatus.Showed;
        }


        /// <summary>
        /// Hide animation method. Use base.HideAnimation(DeltaTime) after animation ends.
        /// </summary>
        /// <param name="DeltaTime">Game delta time.</param>
        protected virtual void HideAnimation(float DeltaTime)
        {
            if (Status == OverlayStatus.Hidding)
            {
                Status = OverlayStatus.Hided;
                Hide();
            }
               
        }

        /// <summary>
        /// Update method.
        /// </summary>
        /// <param name="DeltaTime">Game delta time.</param>
        public virtual void Update(float DeltaTime)
        {
            if (Status == OverlayStatus.Showing)
                ShowAnimation(DeltaTime);
            if (Status == OverlayStatus.Hidding)
                HideAnimation(DeltaTime);
        }

        /// <summary>
        /// Draw method.
        /// </summary>
        public abstract void Draw();

        /// <summary>
        /// Show method.
        /// </summary>
        /// <param name="ParentScreen">Screen, who calls overlay.</param>
        public virtual void Show(Screen ParentScreen)
        {
            if (Status != OverlayStatus.Showing && Status != OverlayStatus.Showed)
            {
                Status = OverlayStatus.Showing;
                Initialize();
                this.ParentScreen = ParentScreen;
            }
        }

        /// <summary>
        /// Hide method.
        /// </summary>
        protected virtual void Hide()
        {
            if (Status != OverlayStatus.Hidding && Status != OverlayStatus.Hided)
                Status = OverlayStatus.Hidding;

            if (Status == OverlayStatus.Hided)
            {
                Status = OverlayStatus.None;
                ParentScreen.Overlays.Remove(this);
                ParentScreen = null;
            }
        }
            

    }
}
