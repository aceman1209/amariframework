﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amari.Animation
{
    public class AnimationController
    {
        Animation[] Animations;
        string CurrentAnimationName = "";

        public AnimationController(Animation[] animationsList)
        {
            Animations = animationsList;
        }

        public void SwitchAnimation(string Name)
        {
            CurrentAnimationName = Name;
        }

        public void Update(float DeltaTime)
        {
            foreach (var animation in Animations)
            {
                if (animation.Name.ToLower() == CurrentAnimationName.ToLower())
                    animation.Update(DeltaTime);
            }
        }
    }
}
