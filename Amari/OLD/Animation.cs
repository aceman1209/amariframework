﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SFML.Graphics;
using SFML.System;

namespace Amari.Animation
{
    public class Animation
    {
        private uint FramesCount;
        private Sprite spr;

        bool Repeat;
        uint RepeatStart, RepeatEnd;
        float CurrentFrame;
        IntRect FrameSize;
        float Speed;
        bool Reverse;

        public string Name { get; private set; }

        public Animation(uint FramesCount, Sprite spr, float Speed, string Name, bool Reverse, bool Repeat = false, uint RepeatStart = 0, uint RepeatEnd = 0)
        {
            this.FramesCount = FramesCount;
            this.spr = spr;
            this.Speed = Speed;
            this.Repeat = Repeat;
            this.RepeatStart = RepeatStart;
            if (RepeatEnd == 0)
                this.RepeatEnd = FramesCount;
            else
                this.RepeatEnd = RepeatEnd - 1;
            FrameSize = spr.TextureRect;
            this.Name = Name;
            this.Reverse = Reverse;
        }

        public void Update(float DeltaTime)
        {
            CurrentFrame += Speed * DeltaTime;
            if (Reverse)
                spr.Scale = new Vector2f(-1f, 1f);
            else
                spr.Scale = new Vector2f(1f, 1f);

            spr.TextureRect = new IntRect(FrameSize.Left * (int)CurrentFrame, FrameSize.Top, FrameSize.Width, FrameSize.Height);

            if (Repeat)
            {
                if (CurrentFrame >= RepeatEnd)
                {
                    CurrentFrame = RepeatStart;
                }
            }
            else
            {
                if (CurrentFrame > FramesCount)
                {
                    CurrentFrame = 0;
                }
            }
            Console.WriteLine($"{(int)CurrentFrame}");
        }
    }
}
